package com.best.wallpapers.hd;

public class DataModel {
    String imageUrl;
    String type;

    DataModel(String imageUrl, String type) {
        this.imageUrl = imageUrl;
        this.type = type;
    }

}